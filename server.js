const express = require('express');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const dotenv = require('dotenv');
path = require('path');

dotenv.config();

const app = express();

const routes = require('./app_server/routes');
const session = require('./app_server/session');
const passport = require('./app_server/auth');
const ioServer = require('./app_server/rtc')(app);

//webrtc franework

var port = process.env.PORT || 3000;
// View engine ejs
app.set('views', path.join(__dirname, 'app_server/views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use(express.static(__dirname + "/static/"));

app.use(session);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());


app.use('/', routes);

// Middleware to catch 404 errors
app.use(function(req, res, next) {
    res.status(404).sendFile(process.cwd() + '/app_server/views/404.htm');
});


ioServer.listen(port);



